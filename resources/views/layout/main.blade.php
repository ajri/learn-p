<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <link rel="stylesheet" href="site/css/style.css">
</head>

<body>
    <div class="header">
        <div class="section-1">
            <div class="container">
                <nav class="navbar navbar-expand-lg ">
                    <div class="container-fluid py-4 my-3 px-0 mx-0">
                        <a class="navbar-brand" href="#">
                            <img src="site/image/logo.png" alt="" srcset="">
                        </a>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse offset-md-1" id="navbarNav">
                            <div class="mr-auto"></div>
                            <ul class="navbar-nav ms-md-4">
                                <li class="nav-item">
                                    <a class="nav-link" aria-current="page" href="/">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="about">About Us</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="product">Products</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="contact">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-md-6 mt-md-4 ms-0 p-0">
                    <h2>Our measure is</h2>
                    <h1>Excellence</h1>
                    <button class="btn">Learn More</button>
                </div>
            </div>
        </div>
        <div class="container-fluid ">
            <div class="text-panel-home ms-md-3">
                <p class="m-md-0 p-md-0"> Partners in Lumber</p>
                <div class="strip p-md-1"></div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container-fluid">
            <div class="text-align-center mt-md-4 pt-md-2">
                <img src="site/image/partner-0.png" alt="" style="width: 96px; height: 75px;">
                <img src="site/image/partner-1.png" alt="" style="width: 176px; height: 66px;">
                <img src="site/image/partner-2.png" alt="" style="width: 258px; height: 66px;">
                <img src="site/image/partner-3.png" alt="" style="width: 230px; height: 87px;">
                <img src="site/image/partner-4.png" alt="" style="width: 126px; height: 81px;">
                <img src="site/image/partner-5.png" alt="" style="width: 272px; height: 101px;">
                <img src="site/image/partner-6.png" alt="" style="width: 122px; height: 97px;">
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>

</html>