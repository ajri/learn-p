<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About Us</title>
    <link rel="stylesheet" href="site/css/about.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
</head>

<body>
    <div class="header">
        <div class="section-1">
            <div class="container">
                <nav class="navbar navbar-expand-lg ">
                    <div class="container-fluid py-4 my-3 px-0 mx-0">
                        <a class="navbar-brand" href="#">
                            <img src="site/image/logo.png" alt="" srcset="">
                        </a>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse offset-md-1" id="navbarNav">
                            <div class="mr-auto"></div>
                            <ul class="navbar-nav ms-md-4">
                                <li class="nav-item">
                                    <a class="nav-link" aria-current="page" href="/">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="about">About Us</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="product">Products</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="contact">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="container-fluid">
            <h1 class="ms-md-5 mb-0 pb-4 pt-3">About Us</h1>
        </div>
    </div>
    <div class="main">
        <div class="section-2 container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="text-panel-about ms-md-5">
                        <p class="pt-3 mb-5">What does excellence mean to us?</p>
                        <h2>Mission</h2>
                        <div class="strip mb-5"></div>
                        <div class="text">
                            Our goal at Caliper Forest Products is to provide high
                            quality,reliable lumber products and services. We
                            understand that our customers require timely, dynamic
                            service through the whole value chain, manufacturers
                            and retailers alike. We deliver on these promises through
                            our expertise in procurement, product knowledge, and
                            logistics. A solutions focused company, Caliper
                            appreciates that each of our customers’ businesses are
                            unique and will have specific needs, we strive to take a
                            tailored, adaptive approach with each customer to
                            satisfy these requirements.
                        </div>
                    </div>
                </div>
                <div class="col-md-6  p-0">
                    <div class="image-mission">
                        <img src="site/image/history-bg.jpg" alt="" srcset="" width="100%">
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: -150px;">
                <div class="col-md-6">
                    <div class="image-history ms-md-5">
                        <img src="site/image/about-box-wood.png" alt="" srcset="" width="90%">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="text-panel ms-md-5">
                        <h2 class="mb-0 pb-0">History</h2>
                        <div class="strip mb-5"></div>
                        <div class="text">
                            Growing up with a father in the lumber business for over
                            forty years, Stephen gravitated to the industry naturally.
                            Through a combination of experience, passion, and
                            entrepreneurial ingenuity, Caliper Forest Products was
                            developed. March 2018 Caliper opened its doors and
                            services supplying SPF and Doug-Fir in a variety of grades
                            to customers that reach across North America.
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="image-history">
                        <img src="site/image/about-2.jpg" alt="" srcset="" width="100%">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="text-panel-value mt-md-5">
                        <div class="img-value">
                            <img src="site/image/about-logo.png" alt="" srcset="">
                        </div>
                        <h2 class="mt-md-2">Value</h2>
                        <div class="strip"></div>
                        <div class="text ms-md-5 mt-md-5 text-center">
                            <p class="mb-md-4">Conducting business with transparency, integrity and due diligence</p>
                            <p class="mb-md-4">Value Creation – Always adapt with ever-changing market conditions to provide continuous innovative solutions to our customers</p>
                            <p class="m-0 p-0">Emphasis on efficiency – time, process, customer focused</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>

</html>