<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About Us</title>
    <link rel="stylesheet" href="site/css/product.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
</head>

<body>
    <div class="header">
        <div class="section-1">
            <div class="container">
                <nav class="navbar navbar-expand-lg ">
                    <div class="container-fluid py-4 my-3 px-0 mx-0">
                        <a class="navbar-brand" href="#">
                            <img src="site/image/logo.png" alt="" srcset="">
                        </a>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse offset-md-1" id="navbarNav">
                            <div class="mr-auto"></div>
                            <ul class="navbar-nav ms-md-4">
                                <li class="nav-item">
                                    <a class="nav-link" aria-current="page" href="/">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="about">About Us</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="product">Products</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="contact">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="container-fluid">
            <div class="title pb-md-3 ms-md-5">
                <h1>Product</h1>
            </div>
        </div>
    </div>
    <div class="main">
        <div class="container-fluid ">
            <div class="row">
                <div class="col-md-6">
                    <div class="text-panel-product pt-md-5 ms-md-5">
                        <h2>Products</h2>
                        <div class="strip mb-md-5"></div>
                        <div class="dimensional w-25 h-25 pb-5">
                            <h1>Dimensional
                                Lumber</h1>
                        </div>
                        <div class="text pb-md-5">
                            <span>Species: SPF, Doug-Fir, Hem-fir, SYP</span>
                            <p class="p-0 m-0">2x3 – 2x12 with lengths 4’-20’</p>
                            <p class="p-0 m-0">Grades: Appearance Grade, MSR, #2&Btr, #3&Btr, Utility, <br>Economy</p>
                        </div>
                        <div class="stud pb-5">
                            <h1>Stud lumber</h1>
                            <div class="text  pb-5">
                                <span>Species: SPF, Doug-Fir, Hem-fir</span>
                                <p class="p-0 m-0">2x3-2x6 in lengths from 8’, 9’, 10’ P.E.T</p>
                                <p class="p-0 m-0">Grades: Appearance Grade, #2&Btr, Stud, Economy</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="dimensional-image mt-md-5 pt-md-5">
                        <img src="site/image/products-1.png" alt="" srcset="" width="90%">
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="cards">
                    <div class="d-flex justify-content-evenly">

                        <div class="card" style="width: 400px;">
                            <img src="site/image/products-2.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <div class="title">
                                    <span>Pallet Stock</span>
                                </div>
                                <div class="text">
                                    <p class="card-text">When it comes to getting your pallets out the door quick with maximum efficiency, pre-cut pallet stock is the answer. From the standard 48” x 40” GMA kits to custom lengths, Caliper has you covered.</p>
                                </div>
                                <b>Species: SPF, Doug-Fir, Hem-fir</b><br>
                                <i>*Custom Remanufacturing also available</i>
                            </div>
                        </div>

                        <div class="card" style="width: 400px; ">
                            <img src="site/image/products-3.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <div class="title">
                                    <span>Territory</span>
                                </div>
                                <div class="text">
                                    <p class="card-text">Caliper Forest Products customer net casts across North America.</p>
                                </div>
                            </div>
                        </div>

                        <div class="card" style="width: 400px;">
                            <img src="site/image/products-4.jpg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <div class="title">
                                    <span>Logistics</span>
                                </div>
                                <div class="text">
                                    <p class="card-text">We here at Caliper know the upmost importance of a timely delivery of your fiber for your business to run at top efficiency. When moving product directly from mills all over North America from a Railcar, Dry Van or Flatdeck volumes, you can rest assured that Calipers trusted logistic partners treat every shipment with top priority.</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="zero text-light">p</div>
            </div>
            <div class="row">
                <div class="zero text-light">p</div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
</body>

</html>